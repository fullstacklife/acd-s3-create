/*

	SOLUTION

	You cannot just copy and paste this because
	the bucket name will need to be your bucket name

	If you run it "as is" it will not work!

	You must replaace <FMI> with your bucket name

	e.g

	2019-03-02-sally-catlostandfoundwebsite

	Keeo the quotes in there below, and literally just 
	replace the characters <FMI>.


*/

var 
    AWS = require("aws-sdk"),
    S3API = new AWS.S3({
        apiVersion: "2006-03-01"
    });                         

(function createBucket(){
    var 
        params = {
            //USE YOUR BUCKET NAME HERE
            Bucket: "<FMI>", 
        };
        S3API.createBucket(params, function(error, data){
            if(error){
                console.log(error);
                throw error;
            }
            console.log("Bucket Created: ", data);
        });
})();